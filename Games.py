from random import randint
import math
import serial
import pygame
from time import sleep
from pygame.locals import *

ser = serial.Serial('COM12', 9600)
pygame.init()
width, height = 960, 540
#width, height = 1920, 1080
animals_count = 7

youwin = pygame.image.load("resources/images/correct.png")
gameover = pygame.image.load("resources/images/incorrect.png")
zero = pygame.image.load("resources/images/0.png")
vinte = pygame.image.load("resources/images/20.png")
quarenta = pygame.image.load("resources/images/40.png")
sessenta = pygame.image.load("resources/images/60.png")
oitenta = pygame.image.load("resources/images/80.png")
cem = pygame.image.load("resources/images/100.png")

font = pygame.font.Font(None, 24)

def get_chars():

    out = ser.readline(10)
    print(int(out))
    return out

def Contagem():

    cont = 0
    x = 0

    while(cont<5):

        screen = pygame.display.set_mode((width, height))
        pygame.display.update()

        res = randint(1,13)

        if (((res % 2 and res % 2 != 1) != 0) or (res < 0) or (res % 11 == 0)):
            continue

        print('Novo numero:' + str(res))


        guide_text = normal_font.render('Select the blocks that corresponds with the numbers of objects on screen', True, (255, 255, 255))
        screen.blit(guide_text, (20, 0))

        #guide2_text = normal_font.render(str(res), True, (255, 255, 255))
        #screen.blit(guide2_text, (20, height/2))



        myspacex = []
        myspacey = []
        image_size = 100
        for i in range (0,res):
            intr = randint(1,animals_count)
            actual_image = pygame.image.load("resources/images/"+str(intr)+".png")
            xr = randint(10,width - image_size -10)
            yr = randint(40,height - image_size -10)
            if i==0:
                myspacex.append(xr)
                myspacey.append(yr)
            else:
                m=0
                while(True):
                    if(((xr > myspacex[m] and xr < myspacex[m]+image_size)
                        or (xr+image_size > myspacex[m] and xr +image_size < myspacex[m]+image_size))
                          and ((yr > myspacey[m] and yr < myspacey[m]+image_size)
                               or (yr+image_size > myspacey[m] and yr+image_size < myspacey[m]+image_size))):
                        xr = randint(10, width - image_size -10)
                        yr = randint(40, height - image_size -10)
                        m=0
                    else:
                        m=m+1
                    if (m==i):
                        break
                myspacex.append(xr)
                myspacey.append(yr)
            screen.blit(actual_image, (xr, yr))

        pygame.display.update()
        '''
        actual_image = pygame.image.load("resources/images/1.png")
        screen.blit(actual_image, (width-100, height-100))
        pygame.display.update()
        '''
        ans = None

        while ans == None:
            ans = int(get_chars())
            #ans = int(input())

        if ans == res:

            screen = pygame.display.set_mode((width, height))
            x = x + 1
            pygame.font.init()
            text = font.render("Question: " + str(cont+1) + " out of 5", True, (0, 100, 0))
            textRect = text.get_rect()
            textRect.centerx = screen.get_rect().centerx
            textRect.centery = screen.get_rect().centery + 100
            screen.blit(youwin, (0, 0))
            screen.blit(text, textRect)
            print('Parabéns você acertou!')
            pygame.display.flip()
            sleep(1.5)

        else:

            screen = pygame.display.set_mode((width, height))
            pygame.font.init()
            text = font.render("Question: " + str(cont+1) + " out of 5", True, (100, 0, 0))
            textRect = text.get_rect()
            textRect.centerx = screen.get_rect().centerx
            textRect.centery = screen.get_rect().centery + 100
            screen.blit(gameover, (0, 0))
            screen.blit(text, textRect)
            pygame.display.flip()
            print('Você errou... Tente novamente!')
            sleep(1.5)

        cont = cont + 1


    return x

def Sequencia():

    cont = 0
    x = 0

    while(cont<5):

        num_1 = randint(0,9)
        num_2 = randint(0,9)
        op = randint(1,3)
        screen = pygame.display.set_mode((width, height))
        pygame.display.update()

        if (op == 1):
            res = math.fabs(((num_1 + num_2) / 2))

            if (((res % 2 and res % 2 != 1) != 0) or (res < 0) or (num_2 % 11 == 0)):
                continue

            print('{} __ {}'.format(num_1, num_2))

            guide_text = normal_font.render('Complete the screen´s sequence', True, (255, 255, 255))
            screen.blit(guide_text, (20, 0))

            guide2_text = question_font.render(str(num_1) + '  __  ' + str(num_2), True, (255, 255, 255))
            screen.blit(guide2_text, (325, height / 2))

            pygame.display.update()

        elif (op == 2):
            res = math.fabs((num_1 - num_2))

            if num_1 > num_2:
                res = (num_2 - res)
            else:
                res = (num_2 + res)

            if (((res % 2 and res % 2 != 1) != 0) or (res < 0) or (num_2 % 11 == 0)):
                continue

            print('{}  {}  __'.format(num_1, num_2))

            guide_text = normal_font.render('Complete the screen´s sequence', True, (255, 255, 255))
            screen.blit(guide_text, (20, 0))

            guide2_text = question_font.render(str(num_1) + '  ' + str(num_2) + ' __', True, (255, 255, 255))
            screen.blit(guide2_text, (325, height / 2))

            pygame.display.update()

        elif (op == 3):
            res = math.fabs(num_2 - num_1)

            if num_1 > num_2:
                res = (num_1 + res)
            else:
                res = (num_1 - res)

            if (((res % 2 and res % 2 != 1) != 0) or (res < 0) or (num_2 % 11 == 0)):
                continue

            print('__  {}  {}'.format(num_1, num_2))

            guide_text = normal_font.render('Complete the screen´s sequence', True, (255, 255, 255))
            screen.blit(guide_text, (20, 0))

            guide2_text = question_font.render('__  ' + str(num_1) + '  ' + str(num_2), True, (255, 255, 255))
            screen.blit(guide2_text, (325, height / 2))

            pygame.display.update()


        ans = None

        while ans == None:
            ans = int(get_chars())
            #ans = int(input())

        if ans == res:
            x = x + 1
            screen = pygame.display.set_mode((width, height))
            pygame.font.init()
            text = font.render("Question: " + str(cont + 1) + " out of 5", True, (0, 100, 0))
            textRect = text.get_rect()
            textRect.centerx = screen.get_rect().centerx
            textRect.centery = screen.get_rect().centery + 100
            screen.blit(youwin, (0, 0))
            screen.blit(text, textRect)
            print('Parabéns você acertou!')
            pygame.display.flip()
            sleep(1.5)

        else:
            screen = pygame.display.set_mode((width, height))
            pygame.font.init()
            text = font.render("Question: " + str(cont + 1) + " out of 5", True, (100, 0, 0))
            textRect = text.get_rect()
            textRect.centerx = screen.get_rect().centerx
            textRect.centery = screen.get_rect().centery + 100
            screen.blit(gameover, (0, 0))
            screen.blit(text, textRect)
            pygame.display.flip()
            print('Você errou... Tente novamente!')
            sleep(1.5)

        cont = cont + 1

    return x

def Equacao():

    cont = 0
    x = 0

    while(cont<5):

        num_1 = randint(0,9)
        num_2 = randint(0,9)
        op = randint(1,4)
        screen = pygame.display.set_mode((width, height))
        pygame.display.update()

        if (op == 1):
            res = (num_2 + num_1)

            if (((res % 2 and res % 2 != 1) != 0) or (res < 0) or (num_2 % 11 == 0)):
                continue

            print('{} + {} = ?'.format(num_1, num_2))

            guide_text = normal_font.render('Solve the screen´s equation', True, (255, 255, 255))
            screen.blit(guide_text, (20, 0))

            guide2_text = question_font.render(str(num_1) + ' + ' + str(num_2) + ' =', True, (255, 255, 255))
            screen.blit(guide2_text, (325, height / 2))

            pygame.display.update()

        elif (op == 2):
            res = (num_1 - num_2)

            if (((res % 2 and res % 2 != 1) != 0) or (res < 0) or (num_2 % 11 == 0)):
                continue

            print('{} - {} = ?'.format(num_1, num_2))

            guide_text = normal_font.render('Solve the screen´s equation', True, (255, 255, 255))
            screen.blit(guide_text, (20, 0))

            guide2_text = question_font.render(str(num_1) + ' - ' + str(num_2) + ' =', True, (255, 255, 255))
            screen.blit(guide2_text, (325, height / 2))

            pygame.display.update()

        elif (op == 3):
            res = (num_1 * num_2)

            if (((res % 2 and res % 2 != 1) != 0) or (res < 0) or (num_2 % 11 == 0)):
                continue

            print('{} x {} = ?'.format(num_1, num_2))

            guide_text = normal_font.render('Solve the screen´s equation', True, (255, 255, 255))
            screen.blit(guide_text, (20, 0))

            guide2_text = question_font.render(str(num_1) + ' * ' + str(num_2) + ' =', True, (255, 255, 255))
            screen.blit(guide2_text, (325, height / 2))

            pygame.display.update()

        elif (op == 4):
            if(num_2 == 0):
                continue

            res = (num_1 / num_2)

            if (((res % 2 and res % 2 != 1) != 0) or (res < 0) or (num_2 % 11 == 0)):
                continue

            print('{} ÷ {} = ?'.format(num_1, num_2))

            guide_text = normal_font.render('Solve the screen´s equation', True, (255, 255, 255))
            screen.blit(guide_text, (20, 0))

            guide2_text = question_font.render(str(num_1) + ' ÷ ' + str(num_2) + ' =', True, (255, 255, 255))
            screen.blit(guide2_text, (325, height / 2))

            pygame.display.update()

        ans = None
        while ans == None:
            ans = int(get_chars())
            #ans = int(input())

        if ans == res:
            x = x + 1
            screen = pygame.display.set_mode((width, height))
            pygame.font.init()
            text = font.render("Question: " + str(cont + 1) + " out of 5", True, (0, 100, 0))
            textRect = text.get_rect()
            textRect.centerx = screen.get_rect().centerx
            textRect.centery = screen.get_rect().centery + 100
            screen.blit(youwin, (0, 0))
            screen.blit(text, textRect)
            print('Parabéns você acertou!')
            pygame.display.flip()
            sleep(1.5)

        else:
            screen = pygame.display.set_mode((width, height))
            pygame.font.init()
            text = font.render("Question: " + str(cont + 1) + " out of 5", True, (100, 0, 0))
            textRect = text.get_rect()
            textRect.centerx = screen.get_rect().centerx
            textRect.centery = screen.get_rect().centery + 100
            screen.blit(gameover, (0, 0))
            screen.blit(text, textRect)
            pygame.display.flip()
            print('Você errou... Tente novamente!')
            sleep(1.5)

        cont = cont + 1

    return x

while(True):

    nota = 0
    screen = pygame.display.set_mode((width, height))

    title_height = 40
    subtitle_height = 30
    question_height = 100
    normal_height = 25

    title_font = pygame.font.SysFont('Comic Sans MS', title_height)
    subtitle_font = pygame.font.SysFont('Comic Sans MS', subtitle_height)
    normal_font = pygame.font.SysFont('Comic Sans MS', normal_height)
    question_font = pygame.font.SysFont('Comic Sans MS', question_height)

    title_text = title_font.render('Mathematical Blocks', True, (255, 255, 255))
    subtitle_text = subtitle_font.render('Select a game with a block:', True, (255, 255, 255))
    question_1_text = normal_font.render('1) Counting Game', True, (255, 255, 255))
    question_2_text = normal_font.render('2) Sequence Game:', True, (255, 255, 255))
    question_3_text = normal_font.render('3) Equation Game:', True, (255, 255, 255))

    screen.blit(title_text, (width/4, 0))
    screen.blit(subtitle_text, (width/4, title_height + 20))
    screen.blit(question_1_text, (20, height/2))
    screen.blit(question_2_text, (20, height/2 + normal_height + 20))
    screen.blit(question_3_text, (20, (height/2 + ((normal_height + 20)*2))))


    pygame.display.flip()


    game = None
    print("digite um valor")

    while game == None:
        game = int(get_chars())
        #game=int(input())

    if (game == 1):
        nota = int(Contagem())

    elif (game == 2):
        nota = int(Sequencia())

    elif (game == 3):
        nota = int(Equacao())

    elif (game == 0):
        break

    else:
        print('Essa opção não é valida')
        continue

    nota = nota * 20
    print('-' * 30)
    print(f'{"Sua nota é: {}":^30}'.format(nota))
    print('-' * 30)

    if (nota == 0):
        screen = pygame.display.set_mode((width, height))
        pygame.font.init()
        screen.blit(zero, (0, 0))
        pygame.display.flip()
        sleep(3)

    elif (nota == 20):
        screen = pygame.display.set_mode((width, height))
        pygame.font.init()
        screen.blit(vinte, (0, 0))
        pygame.display.flip()
        sleep(3)

    elif (nota == 40):
        screen = pygame.display.set_mode((width, height))
        pygame.font.init()
        screen.blit(quarenta, (0, 0))
        pygame.display.flip()
        sleep(3)

    elif (nota == 60):
        screen = pygame.display.set_mode((width, height))
        pygame.font.init()
        screen.blit(sessenta, (0, 0))
        pygame.display.flip()
        sleep(3)

    elif (nota == 80):
        screen = pygame.display.set_mode((width, height))
        pygame.font.init()
        screen.blit(oitenta, (0, 0))
        pygame.display.flip()
        sleep(3)

    elif (nota == 100):
        screen = pygame.display.set_mode((width, height))
        pygame.font.init()
        screen.blit(cem, (0, 0))
        pygame.display.flip()
        sleep(3)
