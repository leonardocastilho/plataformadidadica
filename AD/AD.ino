//#define DEBUG

int c1,c2,c3,c4,c5 = 0;
char cc1,cc2,cc3,cc4,cc5 = 0;        
int outputValue = 0;        

void mensure();
void printvalues();
void resetvalues();
void decide();

#define LIMIT_1_L 170
#define LIMIT_1_H 220
#define LIMIT_2_L 116
#define LIMIT_2_H 210
#define LIMIT_3_L 40
#define LIMIT_3_H 110
#define LIMIT_4_L 650
#define LIMIT_4_H 710
#define LIMIT_5_L 720
#define LIMIT_5_H 780
#define LIMIT_6_L 800
#define LIMIT_6_H 880
#define LIMIT_7_L 580
#define LIMIT_7_H 630
#define LIMIT_8_L 480
#define LIMIT_8_H 520

void setup() 
{
  Serial.begin(9600);
  pinMode(2,INPUT);
}

void loop() 
{  
  mensure();
  if(digitalRead(2)==HIGH)
  {
    printvalues();
    while(digitalRead(2)==HIGH);
  }
  resetvalues();
  delay(20);
}

char decide(int value)
{
    if (value > LIMIT_1_L && value < LIMIT_1_H )
      return  '1';
    else if (value > LIMIT_2_L && value < LIMIT_2_H )
      return  '2';
    else if (value > LIMIT_3_L && value < LIMIT_3_H )
      return  '3';
    else if (value > LIMIT_4_L && value < LIMIT_4_H )
      return  '4';
    else if (value > LIMIT_5_L && value < LIMIT_5_H )
      return  '5';
    else if (value > LIMIT_6_L && value < LIMIT_6_H )
      return  '6';
    else if (value > LIMIT_7_L && value < LIMIT_7_H )
      return  '7';
    else if (value > LIMIT_8_L && value < LIMIT_8_H )
      return  '8';
     return ' ';
}

void mensure()
{
  for (int i=0 ; i<30 ; i++)
  {
    c1 += analogRead(A0);
    c2 += analogRead(A1);
    c3 += analogRead(A2);
    c4 += analogRead(A3);
    c5 += analogRead(A4);
  }
  c1= c1/30;
  c2= c2/30;
  c3= c3/30;
  c4= c4/30;
  c5= c5/30;
  cc1= decide(c1);
  cc2= decide(c2);
  cc3= decide(c3);
  cc4= decide(c4);
  cc5= decide(c5);
}

void printvalues()
{
#ifdef DEBUG
  Serial.print("c1 = ");
  Serial.println(c1);
  Serial.print("caixa 1 = ");
  Serial.println(cc1);
  Serial.print("c2 = ");
  Serial.println(c2);
  Serial.print("caixa 2 = ");
  Serial.println(cc2);
  Serial.print("c3 = ");
  Serial.println(c3);
  Serial.print("caixa 3 = ");
  Serial.println(cc3);
  Serial.print("c4 = ");
  Serial.println(c4);
  Serial.print("caixa 4 = ");
  Serial.println(cc4);
  Serial.print("c5 = ");
  Serial.println(c5);
  Serial.print("caixa 5 = ");
  Serial.println(cc5);
  Serial.println("------------------------------------");
#else
  //Serial.print("!");
  if(cc1 != ' ')
    Serial.print(cc1);
  //Serial.print("&");
  if(cc2 != ' ')
    Serial.print(cc2);
  //Serial.print("&");
  if(cc3 != ' ')
    Serial.print(cc3);
  //Serial.print("&");
  if(cc4 != ' ')
    Serial.print(cc4);
  //Serial.print("&");
  if(cc5 != ' ')
    Serial.print(cc5);
  if(cc1 != ' ' || cc2 != ' ' || cc3 != ' ' || cc4 != ' ' || cc5 != ' ')
    Serial.println();
  //Serial.println("~");
#endif
} 

void resetvalues()
{
  c1=0;
  c2=0;
  c3=0;
  c4=0;
  c5=0;
}

